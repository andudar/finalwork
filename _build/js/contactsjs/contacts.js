(function (){

  var form = document.communicate;

  form.onsubmit = function(){
    var formTrue = false;
    var elems = form.getElementsByClassName('need-to-check');
    for(var i = 0; i < elems.length; i++){
      if(elems[i].value){
        formTrue = true;
      }else {
        show(elems[i]);
        formTrue = false;
      }
    }
    if(!formTrue){
      alert("Form is not right! Please retry again");
      return false;
    }else{
      alert("Everything is alright! Ready to submit");
    }


  };

  form.onkeypress = function(e){
    var target = e.target;
    if(target.classList.contains('need-to-check')){
      hide(target)
    }
  };


  function hide(el){
    if(!el.nextElementSibling.classList.contains('tooltip')){
      return
    }
    el.nextElementSibling.classList.remove('tooltip-show');
  }

  function show(el){
    if(!el.nextElementSibling.classList.contains('tooltip')){
      return
    }

    el.nextElementSibling.classList.add('tooltip-show');
  }

})();