/**
 * Created by Andrew on 11-Mar-16.
 */
(function(){

  var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    paginationClickable: true,
    spaceBetween: 30,
    centeredSlides: true,
    /*autoplay: 2500,*/
    loop: true,
    autoplayDisableOnInteraction: false
  });

  var resume = document.querySelector('.portfolio-content-paper-resume'),
    paper = document.querySelector('.paper-img'),
    resumeTop = parseInt(getComputedStyle(resume).top),
    scrollStart = null,
    scrollEnd = null,
    newTopResume,
    helloButton = document.querySelector('.hello-btn');


  /*resume*/
  window.addEventListener('load',checkResumeOnLoad,false);

  function checkResumeOnLoad(){

    setTimeout(function(){
      if(resume.classList.contains('after-move')){

        resume.style.top = 80 + 'px';
      }
    },100);

  }

  document.onscroll = function(){
    checkResumePosition();

    if(resume.classList.contains('in-move')){

      if(innerHeight - paper.getBoundingClientRect().top > 0 &&
        innerHeight - paper.getBoundingClientRect().top < 100){
        scrollStart = pageYOffset;
        scrollEnd = scrollStart + resume.offsetHeight;
      }

      /* if(innerHeight - paper.getBoundingClientRect().bottom > 0 &&
       -(innerHeight - paper.getBoundingClientRect().bottom) > 100){
       scrollEnd = pageYOffset;
       scrollStart = scrollEnd - resume.offsetHeight;
       }
       */
      newTopResume = resumeTop + (pageYOffset - scrollStart);
      //prevent resume falling off paper when drag and drop scroll bar
      if(newTopResume < 90 && newTopResume > resumeTop){
        resume.style.top = newTopResume + 'px';
      }

    }

  };

  function checkResumePosition(){
    resume.classList.remove('before-move', 'in-move', 'after-move');
    if(paper.getBoundingClientRect().bottom > innerHeight && paper.getBoundingClientRect().top < innerHeight){
      resume.classList.add('in-move');
    }else if(resume.getBoundingClientRect().bottom < innerHeight){
      resume.classList.add('after-move');
    }else if(paper.getBoundingClientRect().top > innerHeight){
      resume.classList.add('before-move');
    }

  }

})();