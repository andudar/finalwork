define(['fillTable','navigation'], function(fillTable, navigation){
  //dynamic paginator, create paginator's items

  var paginator = document.getElementById('paginator'),
    step = navigation.step,
    start = navigation.start,
    end = navigation.end,
    tableContainerLength = 0,
    next = document.querySelector('.next'),
    previous = document.querySelector('.previous');



  function createPaginator(arr){
    tableContainerLength = arr.length;

    if(paginator.children.length > 2){
      removePaginator();
    }
    if(tableContainerLength == 0){
    	next.classList.add('disable');
   		previous.classList.add('disable');
    	return;
    }
    var pageAmount = Math.ceil((arr.length / step));

    for(var i = 1; i < pageAmount+1; i++){
      var span = document.createElement('span');
      span.innerHTML = i;
      span.classList.add('page', 'page-' + i);
      paginator.insertBefore(span,paginator.lastElementChild);
    }

    paginator.querySelector('.page-1').classList.add('table-active-page');

    if(start >= tableContainerLength - step) {
      next.classList.add('disable');
    }
    if(start <= 0) {
      previous.classList.add('disable');
    }
    
  }

  paginator.onclick = function paginatorOnClick(e){
    var currentPage,
      target = e.target;

    if(target.nodeName != 'SPAN'){
      return;
    }

    if(target.closest('.page')){
      clearActivePage();
      removeDisableClass();

      if(start >= 0){
        start = (+target.innerHTML-1) * step;
      }



      end = start + step;
      currentPage = start/step + 1;
      navigation.start = start;
      navigation.end = end;
      if(start >= tableContainerLength - step) {
        next.classList.add('disable');
      }
      if(start <= 0) {
        previous.classList.add('disable');
      }
      fillTable(false);
    }


    if(target.closest('.next')){

      //prevent extra move
      if(start >= tableContainerLength - step){
        return;
      }

      clearActivePage();
      removeDisableClass();
      start += step;
      end = start + step;

      currentPage = start/step + 1;
      navigation.start = start;
      navigation.end = end;

      if(start >= tableContainerLength - step) {
       	next.classList.add('disable');
      }

      fillTable(false);
    }



    if(target.closest('.previous')){

      start = start - step;
      end = end - step;
      //prevent extra move
      if(start < 0){
        start = 0;
        end = 5;
        return;
      }
      clearActivePage();
      removeDisableClass();
      currentPage = start/step + 1;
      navigation.start = start;
      navigation.end = end;

      if(start <= 0) {
        previous.classList.add('disable');
      }

      fillTable(false);
    }

    var nowWeAreHere = paginator.querySelector('.page-' + currentPage);
    if(nowWeAreHere){
      nowWeAreHere.classList.add('table-active-page');
    }
    navigation.start = start;
    navigation.end = end;
  };



  function removePaginator(){
    var pages = paginator.getElementsByClassName('page');
    var arr = [].slice.call(pages);
    arr.forEach(function(item){
      item.parentNode.removeChild(item);
    })
  }

  function clearActivePage(){
    var pages = paginator.querySelectorAll('.page'),
      max = pages.length;

    for(;max--;){
      pages[max].classList.remove('table-active-page');
    }
  }

  function removeDisableClass(){
    previous.classList.remove('disable');
    next.classList.remove('disable');
  }

  return createPaginator;
});