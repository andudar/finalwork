(function () {
	"use strict";
	
	var gulp = require('gulp'),
		sass = require('gulp-sass'),
		autoprefixer = require('gulp-autoprefixer'),
		concat = require('gulp-concat'),
		argv = require('yargs').argv,
		gulpif = require('gulp-if'),
		cssmin = require('gulp-cssmin'),
		htmlmin = require('gulp-htmlmin'),
		uglify = require('gulp-uglify');


	var paths = {
		src: {
		/*	root: './src',*/
      html: './src/*.html',
			css: ['./src/css/vendors/*.css', './src/sass/**/*.scss'],
      fonts: './src/css/fonts/*',
			js: ['./src/js/base/base.js'],
			img: './src/images/**/*'
		},
		build: {
			root: './_build',
			css: './_build/css',
			js: './_build/js/base',
      img: './_build/images',
      fonts: './_build/css/fonts'
		}
		
	};
	
	


	function cssTask(){
		gulp.src(paths.src.css)
		.pipe((sass()))
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
    .pipe(concat('base.min.css'))
    .pipe(gulpif((argv.prod), cssmin()))//after base-min-css!!!
		.pipe(gulp.dest(paths.build.css));
	}

  function fontTask(){
    gulp.src(paths.src.fonts)
      .pipe(gulp.dest(paths.build.fonts));
  }
	function htmlTask(){
    gulp.src(paths.src.html)
      .pipe(gulpif((argv.prod), htmlmin({
        collapseWhitespace:true
      })))
      .pipe(gulp.dest(paths.build.root));
	}
	
	function defaultTask(){//default fire by pressing Enter

		gulp.watch(paths.src.css, ['css']);
		gulp.watch(paths.src.html, ['html']);
		gulp.watch(paths.src.img, ['img']);
		gulp.watch(paths.src.js, ['js']);
		gulp.watch(paths.src.fonts, ['fonts']);
	}

  function imgTask(){
    gulp.src(paths.src.img)
      .pipe(gulp.dest(paths.build.img));
  }

  function jsTask(){
    gulp.src(paths.src.js)
      .pipe(concat('base.min.js'))
      .pipe(gulpif((argv.prod),uglify()))
      .pipe(gulp.dest(paths.build.js));
  }
	
	gulp.task('default',['html', 'css', 'js', 'img', 'fonts'], defaultTask);
	gulp.task('html', htmlTask);
	gulp.task('css', cssTask);
  gulp.task('js', jsTask);
  gulp.task('img', imgTask);
  gulp.task('fonts', fontTask);

})();