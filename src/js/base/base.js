(function () {
  "use strict";

  var headerItems = document.querySelectorAll('.nav-link');

  var header = document.querySelector('.page-header'),
    logo = document.querySelector('.main-logo'),
    nav = document.querySelector('.nav');


  var hamburger = document.querySelector('.hamburger'),
    overlay = document.querySelector(".overlay");

  var pageUpDown = document.querySelector('#pageupdown'),
    overlayContent = document.querySelector('.overlay-content'),
    previousPos;


//fixed header

  document.addEventListener('scroll',fixedHeader);


  function fixedHeader(){
    if(pageYOffset >= header.offsetHeight){
      header.classList.add('header-fixed');
      logo.classList.add('main-logo-fixed');
      nav.classList.add('nav-fixed');
      hamburger.style.position = 'fixed';
      hamburger.style.top = '15px';
      hamburger.style.rigth = '15px';

      if(innerWidth < 979){
        hamburger.style.top = '17px';
      }
      if(innerWidth < 639){
        hamburger.style.top = '27px';
      }
      if(innerWidth < 480){
        hamburger.style.top = '23px';
      }
    }else{
      header.classList.remove('header-fixed');
      logo.classList.remove('main-logo-fixed');
      nav.classList.remove('nav-fixed');
      hamburger.style.top = '';
      hamburger.style.position = '';
    }


  }

//hamburger handler

  hamburger.onclick = function(){
    if(this.classList.contains('active')){
      overlay.style.width = "0%";
      hamburger.style.position = '';
    }else{
      overlay.style.width = "100%";
      hamburger.style.position = 'fixed';
    }
    this.classList.toggle("active");

  };


/* header menu overlay handler */

  overlayContent.onclick = function(e){

    if(e.target.nodeName == 'A'){
      overlay.style.width = "0%";
      hamburger.style.position = '';
      hamburger.classList.remove("active");
    }
  };

  /*page up-down handler*/

  document.addEventListener('scroll',controlPagePosition);

    function controlPagePosition(e){

    if(pageYOffset > document.body.clientHeight ){
      if(pageUpDown.classList.contains('down')){
        pageUpDown.classList.remove('down')
      }

      pageUpDown.classList.add('up');
      pageUpDown.innerHTML = '<i class="fa fa-angle-double-up"></i>';
      previousPos = pageYOffset;
      pageUpDown.onclick = function(e){
        scrollTo(0,0);

      }
    }

    if(pageYOffset == 0 && pageUpDown.classList.contains('up')){
      pageUpDown.classList.remove('up');

      pageUpDown.classList.add('down');
      pageUpDown.innerHTML = '<i class="fa fa-angle-double-down"></i>';
      pageUpDown.onclick = function(e){
        scrollTo(0,previousPos);


      }
    }
    if(pageUpDown.classList.contains('up') && pageYOffset < document.body.clientHeight){
      pageUpDown.classList.remove('up');
    }


  }


})();


